//app.js
const userService = require('./services/user.js');
const api = require('./config/api.js');
App({
  onLaunch: function () {
     //获取用户的登录信息
    let that = this
    userService.checkLogin().then(res => {
      if(res === true) {
        console.log('app login')
        that.globalData.userInfo = wx.getStorageSync('userInfo')
        that.globalData.token = wx.getStorageSync('token')
      } else {
        // 登录
        console.log('app login')
 
        userService.loginByWeixin().then(res2 => {
          try {
            that.globalData.userInfo = wx.getStorageSync('userInfo')
            that.globalData.token = wx.getStorageSync('token')
          } catch (e) {
            console.log('app login 3333', e)
          }
        }).catch((err) => {
          console.log('app login 444', err)
        })

      }
    }).catch((err) => {
      console.log('app login 3333')
      console.log(err)
    })
  },
    
  globalData: {
    userInfo: {
      nickname: 'Hi,游客',
      username: '点击去登录',
      avatar: '/img/avatar/none.png',
      intro: '这货啥都没留下',
      detail: '这货啥都没留下'
    },
    token: '',
  }
})