const recorderManager = wx.getRecorderManager()
let recordOptions = {
  duration: 10000,
  sampleRate: 44100,
  numberOfChannels: 1,
  encodeBitRate: 192000,
  format: 'mp3',
  frameSize: 50
}
const innerAudioContext = wx.createInnerAudioContext()
innerAudioContext.autoplay = true

function init(_this, url, options = recordOptions) {
  recordOptions = options
  recorderManager.onStart(() => {
    console.log('recorder start')
  })
  recorderManager.onResume(() => {
    console.log('recorder resume')
  })
  recorderManager.onPause(() => {
    console.log('recorder pause')
  })
  recorderManager.onStop((res) => {
    console.log('recorder stop', res)
    const { tempFilePath } = res
    // TODO 调用上传文件接口
    uploadQuiz(url, res.tempFilePath, _this)
    _this.setData({
      tmpFilePath: res
    })
  })
  recorderManager.onFrameRecorded((res) => {
    const { frameBuffer } = res
    console.log('frameBuffer.byteLength', frameBuffer.byteLength)
  })
}

function start() {
  recorderManager.start(recordOptions)
}

function stop() {
  recorderManager.stop()
}

function play() {
  // TODO 从api服务器下载文件
  innerAudioContext.src = this.data.tmpFilePath.tempFilePath

  innerAudioContext.onPlay(() => {
    console.log('开始播放')
  })
  innerAudioContext.onError((res) => {
    console.log(res.errMsg)
    console.log(res.errCode)
  })
}

function uploadQuiz(url, filePath, _this) {
  console.log(_this.data, filePath)
  wx.uploadFile({
    url: 'http://192.168.1.105:8998/api/upload',
    filePath: filePath,
    name: 'file',
    formData: {"userId": _this.data.userinfo.id},
    method: 'POST',
    header: {'content-type': 'multipart/form-data',
            'X-Wenda-Token': wx.getStorageSync('token')},
    success: function (res) {
      console.log('res.data:' + res.data)
    },
    fail: function (res) {
      console.log(res)
      wx.showModal({
        title: '提示',
        content: "网络请求失败，请确保网络是否正常",
        showCancel: false,
        success: function (res) {
        }
      })
      wx.hideToast()
    },
    complete: function () {
      // complete
      console.log('complete')
    }
  })
}

module.exports = {
  init,
  start,
  stop,
  play
}