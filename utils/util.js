const api = require('../config/api.js')
const userService = require('../services/user.js')

function formatTime(date) {
  const year = date.getFullYear()
  const month = date.getMonth() + 1
  const day = date.getDate()
  const hour = date.getHours()
  const minute = date.getMinutes()
  const second = date.getSeconds()

  return [year, month, day].map(formatNumber).join('/') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

function formatNumber(n) {
  n = n.toString()
  return n[1] ? n : '0' + n
}

function getUserInfo() {
  return wx.getStorageSync('userInfo')
}
/**
 * 微信request
 */
function request(url, data = {}, method = "GET") {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'X-Wenda-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        console.log("success", res)

        if (res.statusCode == 200) {
          if (res.data.errno == 401) {
            //需要微信登录后才可以操作
            let code = null
            return userService.login().then((res) => {
              console.log('000111', res)
              code = res.code
              return userService.getUserInfo()
            }).then((userInfo) => {
              //登录api服务器
              userService.loginByWeixin().then(res => {
                console.log('0003333', res)
                if (res.data.errno === 0) {
                  console.log(' util.request.loginbyweixin')
                  // TODO 跳转之前页面
                  wx.navigateBack({
                    delta: 2
                  })
                  resolve(res)
                } else {
                  reject(res)
                }
              }).catch((err) => {
                console.log('000444')
                reject(err)
              })
            }).catch((err) => {
              console.log('000555')
              reject(err)
            })
          } else {
            console.log(' 222222222', res.data)
            resolve(res.data)
          }
        } else {
          console.log(' 333333')
          reject(res.errMsg)
        }

      },
      fail: function (err) {
        reject(err)
        console.log("failed")
      }
    })
  });
}

function redirect(url) {
  //判断页面是否需要登录
  if (false) {
    wx.redirectTo({
      url: '/pages/auth/login/login'
    });
    return false;
  } else {
    wx.redirectTo({
      url: url
    });
  }
}

function showErrorToast(msg) {
  wx.showToast({
    title: msg,
    image: '/static/images/icon_error.png'
  })
}

module.exports = {
  formatTime,
  request,
  redirect,
  showErrorToast,
  getUserInfo
}
