const imgRootUrl = 'http://192.168.1.105:8080/static/'
const apiRootUrl = 'http://192.168.1.105:8998/api'

module.exports = {
  imgRootUrl: imgRootUrl,
  apiRootUrl: apiRootUrl,
  AuthLoginByWeixin: apiRootUrl + '/auth/wxlogin',
  auth: apiRootUrl + '/auth/',
  feeds: apiRootUrl + '/feeds/',
  issue: apiRootUrl + '/issue/',
  issueAdd: apiRootUrl + '/issue/add',
  issueUser: apiRootUrl+ '/issue/user',
  answerAdd: apiRootUrl + '/answer/add/',
  answerUser: apiRootUrl + '/answer/user/',
  authRegist: apiRootUrl + '/auth/regist/',
  authLogin: apiRootUrl + '/auth/login/',
  user: apiRootUrl + '/user/',
  hotPerson: apiRootUrl + '/hot/person/',
  relationship: apiRootUrl + '/relationship',
  relationshipCheck: apiRootUrl + '/relationship/check',
  relationshipChg: apiRootUrl + '/relationship/chg'

}
