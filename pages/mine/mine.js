// pages/mine/mine.js
//const app = getApp()
const UserInfo = wx.getStorageSync('userInfo')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userInfo: {},
    myProfile: [{ "desc": "我的关注", "id": "follow" }, { "desc": "我问", "id": "myQues" }, { "desc": "我答", "id": "myAnswer" }],
    myAccount: ["手机号码", "帮助", "结算说明", "关于分答"]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('onLoad')
    var that = this
    console.log(UserInfo)
    this.setData({
      userInfo: UserInfo
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  onInquire: function (event) {
    console.log(event)
    if(event.currentTarget.id==="myQues") {
      wx.navigateTo({
        url: '/pages/profile/profile?type=myQues&id=' + UserInfo.id,
      })
    } else if (event.currentTarget.id === "myAnswer") {
      wx.navigateTo({
        url: '/pages/profile/profile?type=myAnswer&id=' + UserInfo.id,
      })
    } else if (event.currentTarget.id === "follow") {
      // TODO 我的关注
      wx.navigateTo({
        url: '/pages/profile/profile?type=follow&id=' + UserInfo.id,
      })
    }
  }
})