const utils = require('../../utils/util.js')
const api = require('../../config/api.js')
const UserInfo = wx.getStorageSync('userInfo')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBaseUrl: api.imgRootUrl,
    personId: 0,
    isFollow: false,
    person: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let personId = options.id
    let that = this
    console.log('OnLoad', personId)
    utils.request(api.user + '?id=' + personId, {}, 'POST').then((person) => {
      console.log('res:', person)
      console.log('userinfo:', UserInfo)
      // TODO 查看是否关注
      utils.request(api.relationshipCheck + '?type=0&follow_id=' + personId + '&id=' + UserInfo.id, {}, 'POST').then((relationship) => {
        that.setData({
          isFollow: relationship.data.flag,
          personId: options.id,
          person: person.data.user
        })
      }).catch((err) => {
        console.log(err)
      })
    }).catch((err) => {

    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
    
  },

  onFollow: function (event) {
    // TODO 发起follow
    console.log('follow啦！')
    console.log('OnLoad', this.data.personId)
    utils.request(api.relationshipChg + '?action=1&type=0&follow_id=' + this.data.personId + '&id=' + UserInfo.id, {}, 'POST').then((relationship) => {
      console.log(relationship)
      // TODO
      that.setData({
        isFollow: relationship.data.flag
      })
    })
  }
})