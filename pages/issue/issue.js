// pages/issue/issue.js
const api = require('../../config/api.js')
const utils = require('../../utils/util.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBaseUrl: api.imgRootUrl,
    issue: null,
    answerList: [],
    owner: null,
    isShowForm: false
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('OnLoad')
    console.log(options.id)
    this.flushData(options)
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  flushData: function (options) {
    let that = this
    utils.request(api.issue + '?id=' + options.id, {}, 'POST').then((res) => {
      console.log(res)
      if (res.errno === 0) {
        that.setData({
          answerList: res.data.answer,
          owner: res.data.owner,
          issue: res.data.issue
        })
        try {
          wx.setStorageSync('answerList', res.data)
        } catch (error) {
          console.log(error)
        }
      } else {
        console.log('error')
      }
    }).catch((err) => {
      console.log('error', err)
    })
  },

  bindFormSubmit: function (e) {
    if (e.detail.value.textarea === "") {
      wx.showToast({
        title: '请补充信息',
        icon: 'succes',
        duration: 2000,
        mask: true
      })
      return
    }
    try {
      let that = this
      let userinfo = wx.getStorageSync('userinfo')
      utils.request(api.answerAdd + '?owner=' + userinfo.user.id + '&text=' + e.detail.value.textarea + '&issue_id=' + this.data.issue.id, {}, 'POST').then((res) => {
          console.log('wooo: ',res)
          that.flushData({ id: that.data.issue.id })
        }).catch((err) => {
          console.log('err: ', err)
        })
      wx.showToast({
        title: '成功',
        icon: 'succes',
        duration: 2000,
        mask: true
      })
      this.setData({
        isShowForm: !this.data.isShowForm
      })
    } catch (e) {
      console.log(e)
    }
  },
  onShowForm: function () {
    let that = this
    wx.getStorage({
      key: 'userinfo',
      success: function (res) {
        console.log("11111", res.data)
        that.setData({
          isShowForm: !that.data.isShowForm
        })
      },
      fail: function (error) {
        console.log(error)
        wx.navigateTo({
          url: '../login/login'
        })
      }
    })
  }
})