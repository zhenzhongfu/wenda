const utils = require("../../utils/util.js")
const api = require('../../config/api.js')
const WxSearch = require('../../wxSearch/wxSearch.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    hotPerson: []
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    let that = this
    console.log('OnLoad')
    //初始化的时候渲染wxSearchdata
    WxSearch.init(that, 43, ['weappdev', '小程序', 'wxParse', 'wxSearch', 'wxNotification'])
    WxSearch.initMindKeys(['weappdev.com', '微信小程序开发', '微信开发', '微信小程序'])
    utils.request(api.hotPerson, {}, 'POST').then((res) => {
      console.log(res)
      // TODO
      that.setData({
        hotPerson: res.data.hot
      })
    }).catch((err) => {
      console.log(err)
    })
  },

  toPerson: function (event) {
    wx.navigateTo({
      url: '../person/person?id=' + event.currentTarget.dataset.person
    })
  },

  wxSearchFn: function (e) {
    var that = this
    WxSearch.wxSearchAddHisKey(that)
    console.log('gogogogogogo')
    // TODO查询搜索结果
  },
  wxSearchInput: function (e) {
    var that = this
    WxSearch.wxSearchInput(e, that)
  },
  wxSerchFocus: function (e) {
    var that = this
    WxSearch.wxSearchFocus(e, that)
  },
  wxSearchBlur: function (e) {
    var that = this
    WxSearch.wxSearchBlur(e, that)
  },
  wxSearchKeyTap: function (e) {
    var that = this
    WxSearch.wxSearchKeyTap(e, that)
  },
  wxSearchDeleteKey: function (e) {
    var that = this
    WxSearch.wxSearchDeleteKey(e, that)
  },
  wxSearchDeleteAll: function (e) {
    var that = this;
    WxSearch.wxSearchDeleteAll(that)
  },
  wxSearchTap: function (e) {
    var that = this
    WxSearch.wxSearchHiddenPancel(that)
  }

})