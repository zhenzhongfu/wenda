// pages/quiz/quiz.js
const utils = require("../../utils/util.js")
const api = require("../../config/api.js")
const record = require("../../utils/record.js")

Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgBaseUrl: api.imgRootUrl,
    userinfo: null,
    isSpeaking: false,
    tmpFilePath: ''
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    record.init(this, '')
    let that = this
    wx.getStorage({
      key: 'userInfo',
      success: function (res) {
        console.log("11111", res.data)
        that.setData({
          userinfo: res.data
        })
      },
      fail: function (error) {
        console.log(error)
        wx.navigateTo({
          url: '/pages/login/login'
        })
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  bindQuizSubmit: function (e) {
    if (e.detail.value.textarea === "") {
      wx.showToast({
        title: '请补充信息',
        icon: 'succes',
        duration: 2000,
        mask: true
      })
      return
    }
    try {
      let userinfo = this.data.userinfo
      let that = this
      utils.request(api.issueAdd + '?question=' + e.detail.value.textarea + '&owner=' + userinfo.id + '&type=1', {}, 'POST').then((res) => {
        console.log('wooo: ', res)
        if(res.errno === 0) {
          wx.showToast({
            title: '成功',
            icon: 'succes',
            duration: 2000,
            mask: true
          })
          setTimeout(that.goTo, 2000)
        } else {
          console.log("error:", res.errno)
        }
      })
    } catch (err) {
        console.log("error:", err)
    }
  },

  goTo: function () {
    wx.navigateBack({
      delta: 10
    })
  },

  onRecord: function() {
    console.log("mp3Recorder.start")
    this.setData({
      isSpeaking: true
    })
    record.start()
  },

  offRecord: function () {
    console.log("mp3Recorder.stop")
    this.setData({
      isSpeaking: false,
    })
    record.stop()
  },

  onPlay: function() {
    console.log('tmppath:', this.data.tmpFilePath)

    record.play()
  }
})