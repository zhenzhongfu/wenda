// pages/profile/profile.js
const utils = require('../../utils/util.js')
const api = require('../../config/api.js')
const UserInfo = wx.getStorageSync('userInfo')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    userinfo: {},
    imgBaseUrl: '',
    profileType: 'myQues',
    questionList: [],
    answerList: [],
    followList: [],
    hidden: true
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('111', options)
    this.setData({
      userinfo: UserInfo,
      imgBaseUrl: api.imgRootUrl
    })
    if(options.type === 'myQues') {
      wx.setNavigationBarTitle({
        title: '我的问题',
        success: function () {
          console.log("success");
        },
        complete: function () {
          console.log("动态修改微信小程序的页面标题-complete");
        }
      })
      this.setData({
        profileType:'myQues'
      })
      this.getIssueList()
    } else if(options.type === 'myAnswer') {
      wx.setNavigationBarTitle({
        title: '我的答案',
        success: function () {
          console.log("success");
        },
        complete: function () {
          console.log("动态修改微信小程序的页面标题-complete");
        }
      })
      this.setData({
        profileType: 'myAnswer'
      })
      this.getAnswerList()
    } else if(options.type === 'follow') {
      wx.setNavigationBarTitle({
        title: '我的关注',
        success: function () {
          console.log("success");
        },
        complete: function () {
          console.log("动态修改微信小程序的页面标题-complete");
        }
      })
      this.setData({
        profileType: 'follow'
      })
      this.getFollowList()
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('downRefresh:', this.data.requestFlag)
    if (this.data.requestFlag === false) {
      this.data.requestFlag = true
      this.setData({
        hidden: false
      })
      let that = this
      setTimeout(that.getFeeds, 1000)
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  getIssueList: function () {
    let that = this
    utils.request(api.issueUser + '?id=' + UserInfo.id, {}, 'POST').then((res) => {
      console.log("00000 ", res.data)
      if (res.errno === 0) {
        that.setData({
          questionList: res.data.issueList,
          requestFlag: false,
          hidden: true
        })
      } else {
        console.log('error')
      }
    }).catch((err) => {
      console.log('error')
    })
  },

  getAnswerList: function () {
    let that = this
    utils.request(api.answerUser + '?id=' + UserInfo.id, {}, 'POST').then((res) => {
      console.log("00000 ", res.data)
      if (res.errno === 0) {
        that.setData({
          answerList: res.data.list,
          requestFlag: false,
          hidden: true
        })
      } else {
        console.log('error')
      }
    }).catch((err) => {
      console.log('error')
    })
  },

  getFollowList: function () {
    let that = this
    utils.request(api.relationship + '?id=' + UserInfo.id, {}, 'POST').then((res) => {
      console.log("00000 ", res.data)
      if (res.errno === 0) {
        that.setData({
          followList: res.data.relationship,
          requestFlag: false,
          hidden: true
        })
      } else {
        console.log('error')
      }
    }).catch((err) => {
      console.log('error')
    })
  },

  unFollow: function (event) {
    // TODO 发起follow
    console.log('follow啦！', event.currentTarget.dataset)
    let dataset = event.currentTarget.dataset
    let that = this
    utils.request(api.relationshipChg + '?action=0&type=0&follow_id=' + dataset.id + '&id=' + UserInfo.id, {}, 'POST').then((res) =>{
      console.log(res)
      // TODO
      wx.showToast({
        title: '取消成功',
        icon: 'succes',
        duration: 2000,
        mask: true
      })
      let goback = function () {
        wx.navigateBack({
          delta: 1
        })
      }
      setTimeout(goback, 2000)
    }).catch((err) => {
      console.log('error')
    })
  }
})