// pages/feeds/feeds.js
var appInstance = getApp()
console.log(appInstance.globalData)
const utils = require('../../utils/util.js')
const api = require('../../config/api.js')
const userService = require('../../services/user.js')
Page({

  /**
   * 页面的初始数据
   */
  data: {
    feedsList:[],
    requestFlag: false,
    hidden: true,
    imgBaseUrl: api.imgRootUrl
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log('OnLoad')
    let that = this
    utils.request(api.feeds, {}, 'POST').then((res) => {
      console.log(res.data)
      if (res.errno === 0) {
        that.setData({
          feedsList: res.data
        })
        try {
          wx.setStorageSync('feeds', res.data)
        } catch (error) {
          console.log(error)
        }
      } else {
        console.log('error')
      }
    }).catch((err) => {
      console.log('error')
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    console.log('downRefresh:', this.data.requestFlag)
    if (this.data.requestFlag === false) {
      this.data.requestFlag = true
      this.setData({
        hidden: false
      })
      let that = this
      setTimeout(that.getFeeds, 1000)
    }
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    console.info('upRefresh')
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  },

  upper: function () {

  },

  lower: function () {

  },

  getFeeds: function ()  {
    console.log('sdfadsfasdfad')
    let that = this
    utils.request(api.feeds, {}, 'POST').then((res) => {
      let feedsStrorage = wx.getStorageSync('feeds') || []
      console.log(feedsStrorage, res.data.data)
      //TODO feed流还要设计一下
      //feedsStrorage = feedsStrorage.concat(res.data.data)
      feedsStrorage = res.data.data
      that.setData({
        feedsList: feedsStrorage,
        hidden: true,
        requestFlag: false
      })
      try {
        wx.setStorageSync('feeds', feedsStrorage)
      } catch (e) { 
        console.log(e)
      }
      console.log("同步成功啦")
    }).catch((err) => {
      console.log(err)
    })
  },

  toPerson: function (event) {
    console.log(event)
    wx.navigateTo({
      url: '../person/person?id=' + event.target.dataset.personId
    })
  },

  onQuiz: function () {
    console.log("quiz")
    wx.navigateTo({
      url: '../quiz/quiz'
    })
    /*
    console.log(wx.getStorageSync('userInfo'))
    // TODO 判断是否登录过
    if(wx.getStorageSync('userInfo')!=null && wx.getStorageSync('token')!=null) {
      wx.navigateTo({
        url: '../quiz/quiz'
      })
    } else {
      // TODO 登录
      userService.loginByWeixin().then((res) => {
        wx.navigateTo({
          url: '../quiz/quiz'
        })
      }).catch((err) => {
        console.log(err)
      })
     
    } */
  }

})