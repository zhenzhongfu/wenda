/**
 * 用户相关服务
 */

const api = require('../config/api.js')

/**
 * 微信request
 */
function request(url, data = {}, method = "GET") {
  return new Promise(function (resolve, reject) {
    wx.request({
      url: url,
      data: data,
      method: method,
      header: {
        'Content-Type': 'application/json',
        'X-Wenda-Token': wx.getStorageSync('token')
      },
      success: function (res) {
        console.log("success", res);

        if (res.statusCode == 200) {
          if (res.data.errno == 401) {
            //需要微信登录后才可以操作
            let code = null;
            return login().then((res) => {
              console.log('000111', res)
              code = res.code;
              return getUserInfo();
            }).then((userInfo) => {
              //登录api服务器
              loginByWeixin().then(res => {
                console.log('0003333')
                if (res.errno === 0) {
                  console.log('request.loginbyweixin')
                  resolve(res);
                } else {
                  reject(res);
                }
              }).catch((err) => {
                console.log('000444')
                reject(err);
              });
            }).catch((err) => {
              console.log('000555')
              reject(err);
            })
          } else {
            console.log(' 222222222', res.data)
            resolve(res.data);
          }
        } else {
          console.log(' 333333')
          reject(res.errMsg);
        }

      },
      fail: function (err) {
        reject(err)
        console.log("failed")
      }
    })
  });
}

/**
 * 检查微信会话是否过期
 */
function checkSession() {
  return new Promise(function (resolve, reject) {
    wx.checkSession({
      success: function () {
        resolve(true);
      },
      fail: function () {
        reject(false)
      }
    })
  });
}

/**
 * 调用微信登录
 */
function login() {
  return new Promise(function (resolve, reject) {
    wx.login({
      success: function (res) {
        if (res.code) {
          //登录远程服务器
          console.log(res)
          resolve(res);
        } else {
          reject(res);
        }
      },
      fail: function (err) {
        reject(err);
      }
    });
  });
}

function getUserInfo() {
  return new Promise(function (resolve, reject) {
    wx.getUserInfo({
      withCredentials: true,
      success: function (res) {
        console.log(res)
        resolve(res);
      },
      fail: function (err) {
        reject(err);
      }
    })
  });
}

/**
 * 调用微信登录
 */
function loginByWeixin() {
  let code = null
  return new Promise(function (resolve, reject) {
    return login().then((res) => {
      code = res.code
      return getUserInfo()
    }).then((userInfo) => {
      //登录远程服务器
      wx.request({
        url: api.AuthLoginByWeixin,
        data: { code: code, userInfo: userInfo },
        method: 'POST',
        header: {
          'Content-Type': 'application/json',
          'X-Wenda-Token': wx.getStorageSync('token')
        },
        success: function (res) {
          console.log("success", res)
          if (res.data.errno === 0) {
            console.log("0000000000000000", res.data.data.token)
            //存储用户信息, TODO 这里将微信的userinfo和user表合并
            wx.setStorageSync('userInfo', res.data.data.userinfo)
            wx.setStorageSync('token', res.data.data.token)
            resolve(res)
          } else {
            console.log("111111111111111111:", res)
            reject(res)
          }
        },
        fail: (err) => {
          reject(err)
        }
      })
    }).catch((err) => {
        console.log("222222222222222")
        reject(err)
    })
  })
}

/**
 * 判断用户是否登录 */
function checkLogin() {
  return new Promise(function (resolve, reject) {
    if (wx.getStorageSync('userInfo') && wx.getStorageSync('token')) {
      checkSession().then(() => {
        resolve(true)
      }).catch(() => {
        console.log("0000")
        reject(false)
      })
    } else {
      console.log("1111")
      resolve(false);
    }
  });
}


module.exports = {
  loginByWeixin,
  checkLogin,
  checkSession,
  login,
  getUserInfo
}